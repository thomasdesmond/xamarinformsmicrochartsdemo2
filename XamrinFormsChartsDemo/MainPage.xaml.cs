﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace XamrinFormsChartsDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            CreateCharts();
        }

        private void CreateCharts()
        {
            var entryList = new List<Entry>()
            {
                 new Entry(200)
                {
                    Label = "Grade A",
                    Color = SKColor.Parse("#234432"),
                },
                new Entry(400)
                {
                    Label = "Grade B",
                    Color = SKColor.Parse("#2FF234"),
                },
                new Entry(200)
                {
                    Label = "Grade C",
                    Color = SKColor.Parse("#23CCC2"),
                }
            };
            var barChart = new BarChart() { Entries = entryList };
            var donutChart = new DonutChart() { Entries = entryList };

            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
        }


        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Chart1.IsVisible = !Chart1.IsVisible;
        }
    }
}
